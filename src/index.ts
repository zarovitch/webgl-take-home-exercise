import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import Stats from 'stats.js';
import * as dat from 'dat.gui';

class Viewer {
  stats = new Stats();
  camera: THREE.PerspectiveCamera;
  controls: OrbitControls;
  renderer: THREE.WebGLRenderer;
  scene = new THREE.Scene();
  raycaster = new THREE.Raycaster();
  selectedPoints = [];
  highlightingPoints = true; // [AB] need better calling to avoid confusion with highlightPoints()

  constructor(canvasElement: HTMLCanvasElement) {
    this.renderer = new THREE.WebGLRenderer({
      canvas: canvasElement,
      antialias: true,
    });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    // camera setup
    this.camera = new THREE.PerspectiveCamera(
      60,
      window.innerWidth / window.innerHeight,
      1,
      10000
    );
    this.camera.up.set(0, 0, 1);

    // control setup
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    // [AB] GUI setup
    const gui = new dat.GUI();
    gui.width = 350;

    gui.add(this, 'highlightingPoints')
      .name('Highlight selected points')
      .onChange(() => {
        if (!this.highlightingPoints) {
          this.selectedPoints.forEach(points => this.highlightPoints(points, true));
        }
      });

    // [AB] raycaster setup
    this.raycaster.params.Points.threshold = 0.05;
    gui.add(this.raycaster.params.Points, 'threshold', 0.01, 0.5, 0.01)
      .name('Raycaster threshold');

    // stats setup
    this.stats.showPanel(0); // display fps
    document.body.appendChild(this.stats.dom);

    // listen for resize events
    window.addEventListener('resize', () => {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    });

    // listen for click events
    window.addEventListener('click', event => this.selectPoint(event));
  }

  // don't call multiple times
  render(): void {
    this.stats.begin();
    this.renderer.render(this.scene, this.camera);
    this.stats.end();
    requestAnimationFrame(() => this.render());
  }

  // helper method for setting camera and controls
  setCameraControls(options: {
    userPosition: THREE.Vector3;
    lookAtPoint: THREE.Vector3;
    far: number;
  }) {
    const {userPosition, lookAtPoint, far} = options;

    // set far plane & position
    this.camera.far = far;
    this.camera.position.copy(userPosition);
    this.camera.updateMatrixWorld();

    // set look at point
    this.controls.target = lookAtPoint;
    this.controls.maxDistance = 10000000; // magic big number
    this.controls.saveState();
    this.controls.update();
  }

  selectPoint(event: MouseEvent) {
    // [AB] normalize mouse coordinates to use the raycaster
    const normalizedCoordinate = new THREE.Vector2();
    normalizedCoordinate.setX((event.clientX / window.innerWidth) * 2 - 1);
    normalizedCoordinate.setY( -(event.clientY / window.innerHeight) * 2 + 1);

    // [AB] lazy option, can be terrible if there are a lot of points, it should
    // be better to pick points with the GPU by rendering a partial section of
    // the canvas off screen (it could be the opportunity to add a radius to the
    // picked area)
    this.raycaster.setFromCamera(normalizedCoordinate, this.camera);
    const intersects = this.raycaster.intersectObjects(this.scene.children, true);

    if (intersects.length == 0) {
      console.log('No point selected');
      return;
    }

    console.log('List of selected points', intersects);

    // [AB] store the 2 selected points for measuring purpose
    if (this.selectedPoints.length == 2) {
      this.highlightPoints(this.selectedPoints.shift(), true);
    }
    this.selectedPoints.push(intersects);

    // [AB] first highlight
    if (this.highlightingPoints) {
      this.highlightPoints(intersects);
    }

    if (this.selectedPoints.length == 2) {
      this.measureDistanceBetweenPoints();
    }
  }

  highlightPoints(points: any[], clean = false) {
    points.forEach(p => {
      const index = p.index * 4;
      if (!clean) {
        // [AB] don't forget to save the original color
        p.previousColor = p.object.geometry.attributes.color.array
          .slice(index, index + 3);
        p.object.geometry.attributes.color.set([255, 50, 50], index);
      } else {
        p.object.geometry.attributes.color.set(p.previousColor, index);
      }

      p.object.geometry.attributes.color.needsUpdate = true;
    });
  }

  measureDistanceBetweenPoints() {
    // [AB] compute the distance between the mean of the two points
    const meanPointA = new THREE.Vector3();
    this.selectedPoints[0].forEach(s => meanPointA.add(s.point));
    meanPointA.divideScalar(this.selectedPoints[0].length);

    const meanPointB = new THREE.Vector3();
    this.selectedPoints[1].forEach(s => meanPointB.add(s.point));
    meanPointB.divideScalar(this.selectedPoints[1].length);

    console.log(`${meanPointA.distanceTo(meanPointB).toFixed(3)} meters`);
  }

  async loadModelAndDisplay(url: string) {
    const model = await this.loadGLTFAsync(url);
    this.scene.add(model);

    // [AB] get the global geometry bouding box and use it to get an approximate
    // position for the camera to move to and focus on. The model could be moved
    // but I don't want to modify the geometry to avoid losing precision in the
    // process, and translating the whole model would not be a good option imho.
    const bbox = new THREE.Box3();
    // (adding ts-ignore because of `geometry` which does not pass 🤨)
    // @ts-ignore
    model.children[0].children.forEach(c => bbox.union(c.geometry.boundingBox));

    const lookAtPoint = new THREE.Vector3();
    bbox.getCenter(lookAtPoint);
    const size = new THREE.Vector3();
    bbox.getSize(size);

    this.setCameraControls({
      // [AB] use a corner of the bbox and translate diagonally using its size
      userPosition: bbox.max.clone().add(size),
      lookAtPoint,
      // [AB] far can be adapted to the model size
      far: Math.max(2.0 * size.length(), this.camera.far)
    });
  }

  // boo callbacks, yay promises
  async loadGLTFAsync(url: string): Promise<THREE.Group> {
    return new Promise((resolve, reject) => {
      new GLTFLoader().load(
        url,
        ({scene}) => resolve(scene),
        () => {},
        err => reject(err)
      );
    });
  }
}

const SMALL_CLOUD_GLB = 'assets/small_cloud.glb';
const BIG_CLOUD_GLB = 'assets/big_cloud.glb';

const viewer = new Viewer(
  document.getElementById('viewer') as HTMLCanvasElement
);

// viewer.loadModelAndDisplay(BIG_CLOUD_GLB);
viewer.loadModelAndDisplay(SMALL_CLOUD_GLB);
viewer.render();
