A quick report to explain what I did for this assignement. All work has been
done in `index.ts`, as I felt it was not important enough to justify having
other files.

All my comments in `index.ts` are marked with `[AB]`.

# Goals

## Displaying data on screen

*Time spent: ~45 min*

- Spent a minute to change the way `THREE` is called as it provides better
  visual clues where `THREE` objects are used (totally personal take)
- First thing was to naively set the `lookAt` of the camera to the model but
  after a quick investigation, relevant coordinates were set in children
  geometries.
- With each geometry having its bounding box set, created a bounding box
  embracing all of them and use the center of it as the `lookAt` coordinate.
- Fiddled a little with the created bounding box informations to position the
  camera the best way possible, using `size` for example.

## Selecting points

*Time spent: ~30 min*

- Added a `raycaster` property to `Viewer`.
- Set the ray using normalized mouse coordinates (between -1 and 1)
- Used the `intersectObjects` method to get all the intersected points under the
  cursor/ray. Spent 5 minutes with no result before realising that the
  `recursive` parameter was not set to `true`.
- Played a little with the threshold and choose a `0.05` value which gave
  acceptable results in my opinion.
- Added (afterwards) a selector for the threshold.

## Writing this report and doing some research

*Time spent: ~1h*

# Bonus Goals

## Optimisation

### Data

The biggest performance bottleneck is obviously the data. Having 327MB worth of
points grouped in a sort of single layer is quite a lot to handle in one go and
it could heavily benefit from being split in multiple chunks. At the same time,
chunks could add some LOD which could be used to reduce the amount of points
displayed at the same time.

One downside of changing the data to a format like this (using Potree, EPT or
something else) is the number of files (having hundreds instead of a single one)
and a little increase in size. The solution here would be to use a format which
support HTTP GET range requests, "that let clients ask for just the portions of
a file that they need" (similar to [CogeoTIFF](https://www.cogeo.org/) for
example). This is a thing planned in [Potree 2.0](https://www.cg.tuwien.ac.at/research/publications/2020/SCHUETZ-2020-MPC/SCHUETZ-2020-MPC-paper.pdf).

### Picking method

Instead of using `Raycaster` and going through a lot of `THREE.Points`, use a
GPU rendering of the relevant area to pick points. This will be faster if a lot
of points are rendered (like the big sample provided).

## Change colour of point

*Time spent: ~30 min*

My first train of thoughts was:

- Add a UI (dat.gui) to toggle point highlighting
- In `selectPoint`, add a color mask to the selected points and store those
  points.
- Clean the color mask at the beginning of `selectPoint`.

However simply applying a color mask to the material would result in a whole
package of points to change color. Thus the `color` attribute needs to be
modified. Knowing the index of the point, the color can be easily modified.

After a few more thoughts though, I think it would be better to have a
completely independant point that can be placed on top of the selected points.
No buffer is then modified on the go, and this highlighted point can be moved
whenever the selection changes. Note that I didn't do it as I didn't want to
spend too much time on this.

## Distance between two points

*Time spent: ~20 min*

As there isn't always a single point selected, created a mean point using all
close selected points. Note that points are relatively close, so maybe the mean
point is not necessary and could be ignored.

A line could be added to have a feedback visually on where it is measured,
useful when highlighted points are not quite visible.

The result is displayed in the console.

## Unit tests

I didn't write any, but here is what I would have tested:

- points selection, using something like Puppeteer to simulate mouse click
- points highlighting:
    - check if points are correctly cleaned after having been but out of selection
    - check if points are correctly cleaned after setting
    `this.highlightingPoints` to  `false`.
- measure:
    - check different measurements, especially with the mean system
- GUI interaction (change of threshold, toggle highlighting...) to see if things
  are correctly affected
